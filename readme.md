# 2060 Digital Simple Site Generator

A shell script to speed up the creation of local dev sites. Made specifically to work with WP Engine installs and Mac OS X. 

## Description

This script can do the following things for you:

* Set up site directory (web root)
* Set up log files for site
* Add local domain(s) to /etc/hosts file
* Create virtual host file, including multiple server aliases 
* Enable site in Apache
* Create local git repository, create custom .gitignore file if needed
* Add WP Engine remotes for git push deployments
* Build site from a WP Engine backup ZIP file
* Pull site (using git) from a WP Engine production remote
* Create a proxy for the wp-content/uploads directory pointing to live uploads folder
* Create a MySQL database to use for this site
* Attempt to automatically import MySQL database from a .sql file
* Set up a special wp-config.php file optimized for local development, and using correct database connection details
* Remove WP Engine mu-plugins files which can cause problems on local dev sites

The goal behind this script is to allow you to create a fully-functioning local copy of your site in only a minute or two, without all the hassle of editing multiple files and pulling everything in manually.

## Setup

### Dependencies

You will want to have git installed (visit http://git-scm.com/download/mac).

You will also need MySQL to be installed (visit http://dev.mysql.com/downloads/mysql/). You'll also want to make sure that `mysql` and `mysqladmin` are added to your PATH environment variable (see below).

### MySQL Root Password

This script is set up to generate MySQL connection details inside your wp.config.php file, but it uses the username `root` with the password set to `root` as well (on 127.0.0.1). You will either want to set your MySQL root password to match this, or else modify the script with your own root password. 

### Recommended directories

It's suggested that you go ahead and create the following directories, if they don't exist:

`/private/etc/apache2/sites-available/`

`/private/etc/apache2/sites-enabled/`

`/Library/WebServer/Logs`

`/Library/WebServer/Documents`


### httpd.conf

Your Apache configuration file will need to have the following line added to it in order to enable separate virtual hosts:

`Include /private/etc/apache2/sites-enabled/*.*` 

You may also wish to comment out or delete the following line, if you aren't using httpd-vhosts.conf for anything else:

`Include /private/etc/apache2/extra/httpd-vhosts.conf`

### PATH environment variable and Where to place this script 

It's recommended that you place this script in `~/bin` (where `~` refers to your home folder) and add this directory to your PATH like so:

`export PATH=$PATH:~/bin`

Once it has been added to your PATH variable, you should be able to simply type the `sitegen` command in the terminal to run the script.

To permanently add this directory to your PATH, however, you will need to edit `~/.bash_profile.` This is a good place to add mysqladmin to your PATH as well. For example, type:

`sudo nano ~/bash_profile` 

And give it the following content:

`PATH=$PATH:$HOME/bin:/usr/local/mysql/bin`

Then save and exit (if you're using nano, use Ctrl + X).

### Additional Notes

This script assumes you want to have your web server set up to work a certain way. In particular, it places all of your sites in `/Library/WebServer/Documents`, with each site getting its own folder named after its WP Engine install (e.g., installname.dev). The site folder serves as the web root; the site files are contained directly within this directory, and not within another folder (e.g. public_html). You may want to create a symlink to `/Library/WebServer/Documents` inside your home folder to make it more easily accessible (e.g. via something like `cd ~/Sites`).

Logs are kept separately, in `/Library/WebServer/Logs`. Each site will also have its own log directory, containing both and access log and an error log.

Virtual hosts are stored in `/etc/apache/sites-available`, with each site kept in a separate file, again named after its WP Engine install (e.g., installname.dev). Enabled sites will have a symlink created in `/etc/apache/sites-enabled` which points to its respective file in sites-available; this script automatically enables a site upon creation.

## Usage

Assuming you have set everything up correctly, open a terminal and type `sitegen` (or, if you would prefer to specify your WP Engine install name right here, you may instead type `sitegen installname`, where "installname" is your WP Engine install name).

If you do not enter your install name with the command, you will then be prompted to enter it.

### Server Aliases

You will next be asked to provide one or more server aliases. If you do not have any aliases you'd like to set up, you may simply press enter. Otherwise, enter the aliases one at a time (press enter after each one). You do not need to create aliases for the "www." prefix, as these will be added automatically for both your install name and for each server alias you create.

For example, your WP Engine install name might be "examplesite," which would receive the default local domain of examplesite.dev, but if you knew that the final home for the production site would be examplesitedomainname.com, you could use examplesitedomainname.dev as a server alias. Or, if you didn't want to have to type out examplesite.dev in your URL bar while developing locally, you could technically add an alias for, say, es.dev. Just be careful not to get too carried away here (you don't want conflicts with other sites). 

### Git Repository And Remotes

Next, you will receive a prompt asking if the script should create a local git repository and automatically configure the WP Engine remotes. It's recommended that you do this, unless you have some reason for not using git.

This will run `git init` in your site directory and then add two remotes, where "installname" corresponds to your WP Engine install name:

* Production: git@git.wpengine.com:production/installname.git
* Staging: git@git.wpengine.com:staging/installname.git

### Build From WP Engine Backup

You will then be given the option to build your site from the WP Engine backup ZIP. You'd generally want to do this if you were creating a completely new site where git push deployments had not yet been set up. If you select this option, you will just need to enter the download URL for the WP Engine backup ZIP, which you can obtain by going to your install's backup points in the WP Engine admin area and clicking "Download ZIP."

If you are creating a local dev site for a site that is already managed through git (e.g. a site someone else may have set up), you do not want to choose this option. 

### Pull From Production Remote

If you choose NOT to build your site from the WP Engine backup ZIP, you will be asked if you want to pull from the Production remote. You will probably want to do this, but you will need to be sure that your developer key has been added to this install inside your WP Engine account. Otherwise, you will receive a "fatal: Could not read from remote repository" warning, as you don't have access rights and will be denied. In this case, you will need to come back after your key has been added and manually pull from the remote using the `git pull Production master` command.

### Create a Proxy for wp-content/uploads Directory

If you want your wp-content/uploads directory to redirect to the uploads directory on your production site, this is for you. It prevents the need for keeping your local site uploads in sync with the live site, but if you need to use your local wp-content/uploads directory for whatever reason, it may cause you some problems.

### Creating a MySQL Database

The script will ask you if you would like to create a database for this install; you probably do, unless it already exists.

If you do, it will check to see if an export (SQL dump) of your database exists within your site files. If it does, you will have the option of automatically attempting to import your site database. Otherwise, you will need to manually download a backup of your MySQL database and import it yourself. 

### Finish

That should be it! Your site should now be ready to go at http://installname.dev (where "installname" refers to your WP Engine install name) as well as any aliases you may have set up.